import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestionComponent } from './destion.component';

describe('DestionComponent', () => {
  let component: DestionComponent;
  let fixture: ComponentFixture<DestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
